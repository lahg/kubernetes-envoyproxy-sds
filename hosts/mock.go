package hosts

type MockService struct {
	GetHostsFunc func(serviceName string) ([]*Host, error)
}

var _ Service = &MockService{}

func (s *MockService) GetHosts(serviceName string) ([]*Host, error) {
	if s.GetHostsFunc == nil {
		return nil, nil
	}

	return s.GetHostsFunc(serviceName)
}

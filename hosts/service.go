package hosts

import "fmt"

type Host struct {
	IP   string   `json:"ip_address"`
	Port int      `json:"port"`
	Tags struct{} `json:"tags"`
}

func (h *Host) String() string {
	return fmt.Sprint(*h)
}

type Service interface {
	GetHosts(serviceName string) ([]*Host, error)
}

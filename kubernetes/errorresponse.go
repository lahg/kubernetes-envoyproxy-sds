package kubernetes

import (
	"fmt"
	"net/http"
)

type ErrorResponse struct {
	Response *http.Response
}

func (e *ErrorResponse) Error() string {
	return fmt.Sprintf(
		"%v %v: %d",
		e.Response.Request.Method,
		e.Response.Request.URL,
		e.Response.StatusCode,
	)
}

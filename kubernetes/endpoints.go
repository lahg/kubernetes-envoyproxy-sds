package kubernetes

type endpoints struct {
	Subsets []subset `json:"subsets"`
}

type subset struct {
	Addresses []address `json:"addresses"`
	Ports     []port    `json:"ports"`
}

type address struct {
	IP string `json:"ip"`
}

type port struct {
	Port int `json:"port"`
}

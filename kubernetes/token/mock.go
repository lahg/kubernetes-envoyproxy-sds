package token

type MockService struct {
	GetTokenFunc func() (string, error)
}

var _ Service = &MockService{}

func (s *MockService) GetToken() (string, error) {
	if s.GetTokenFunc == nil {
		return "", nil
	}

	return s.GetTokenFunc()
}

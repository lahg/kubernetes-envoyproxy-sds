package token

type Service interface {
	GetToken() (string, error)
}

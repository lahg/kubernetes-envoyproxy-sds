package kubernetes

import (
	"io/ioutil"
	"os"

	"gitlab.com/lahg/kubernetes-envoyproxy-sds/kubernetes/token"
)

type TokenService struct{}

var _ token.Service = &TokenService{}

func (s *TokenService) GetToken() (string, error) {
	const tokenFileName = "/var/run/secrets/kubernetes.io/serviceaccount/token"
	tokenFile, err := os.Open(tokenFileName)
	if err != nil {
		return "", err
	}

	tokenFileBody, err := ioutil.ReadAll(tokenFile)
	if err != nil {
		return "", err
	}

	return string(tokenFileBody), nil
}

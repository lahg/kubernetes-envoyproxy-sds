package kubernetes

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/lahg/kubernetes-envoyproxy-sds/hosts"
)

const defaultNamespace = "default"

var httpClient = &http.Client{
	Transport: &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	},
}

type HostService struct {
	Namespace string
	Token     string
}

var _ hosts.Service = &HostService{}

func NewHostsService(namespace, token string) (*HostService, error) {
	if token == "" {
		return nil, errors.New("invalid argument `token`: \"\"")
	}

	if namespace == "" {
		namespace = defaultNamespace
	}

	hostService := &HostService{
		Namespace: namespace,
		Token:     token,
	}

	return hostService, nil
}

func NewDefaultHostsService() (*HostService, error) {
	defaultTokenService := &TokenService{}
	token, err := defaultTokenService.GetToken()
	if err != nil {
		return nil, err
	}

	defaultHostService := &HostService{
		Namespace: defaultNamespace,
		Token:     token,
	}

	return defaultHostService, nil
}

func (s *HostService) GetHosts(serviceName string) ([]*hosts.Host, error) {
	resp, err := makeHTTPRequestToKubernetesAPI(s.Namespace, serviceName, s.Token)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if isHTTPResponseAnErrors(resp) {
		return []*hosts.Host{}, nil
	}

	es, err := decodeEndpointsFromHTTPResponse(resp)
	if err != nil {
		return nil, err
	}

	hs := createHostsFromEndpoints(es)

	return hs, nil
}

func isHTTPResponseAnErrors(resp *http.Response) bool {
	return resp.StatusCode < 200 || resp.StatusCode > 299
}

func decodeEndpointsFromHTTPResponse(resp *http.Response) (*endpoints, error) {
	es := &endpoints{}
	if err := json.NewDecoder(resp.Body).Decode(&es); err != nil {
		return nil, err
	}

	return es, nil
}

func makeHTTPRequestToKubernetesAPI(namespace, service, token string) (*http.Response, error) {
	const endpointsURLTemplate = "https://kubernetes/api/v1/namespaces/%s/endpoints/%s"
	endpointsURL := fmt.Sprintf(endpointsURLTemplate, namespace, service)

	req, err := http.NewRequest(http.MethodGet, endpointsURL, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "Bearer "+token)

	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func createHostsFromEndpoints(es *endpoints) []*hosts.Host {
	if len(es.Subsets) == 0 {
		return []*hosts.Host{}
	}

	subset := es.Subsets[0]
	commonPort := subset.Ports[0].Port
	hs := make([]*hosts.Host, len(subset.Addresses))
	for i := range subset.Addresses {
		hs[i] = &hosts.Host{
			IP:   subset.Addresses[i].IP,
			Port: commonPort,
			Tags: struct{}{},
		}
	}

	return hs
}

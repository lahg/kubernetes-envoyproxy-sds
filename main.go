package main

import (
	"log"
	"net/http"

	"github.com/dimfeld/httptreemux"

	"gitlab.com/lahg/kubernetes-envoyproxy-sds/kubernetes"
	"gitlab.com/lahg/kubernetes-envoyproxy-sds/registration"
)

func main() {
	hostsService, err := kubernetes.NewDefaultHostsService()
	if err != nil {
		log.Fatalln("error creating default host service:", err)
	}

	registrationController := &registration.Controller{
		HostsService: hostsService,
	}

	mux := httptreemux.NewContextMux()
	mux.GET("/v1/registration/:serviceName", registrationController.GetHosts)

	log.Println(http.ListenAndServe(":8080", mux))
}

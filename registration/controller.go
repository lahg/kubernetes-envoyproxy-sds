package registration

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/dimfeld/httptreemux"

	"gitlab.com/lahg/kubernetes-envoyproxy-sds/hosts"
)

type Controller struct {
	HostsService hosts.Service
}

func (c *Controller) GetHosts(w http.ResponseWriter, r *http.Request) {
	params := httptreemux.ContextParams(r.Context())
	serviceName := params["serviceName"]

	hs, err := c.HostsService.GetHosts(serviceName)
	if err != nil {
		http.Error(w, "error while getting hosts for service "+serviceName, http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	resp := struct {
		Hosts []*hosts.Host `json:"hosts"`
	}{
		Hosts: hs,
	}
	if err = json.NewEncoder(w).Encode(&resp); err != nil {
		log.Println("error writing response:", err)
	}
}
